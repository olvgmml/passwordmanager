'use strict';

function setCookie(key, value, expires) {
  var d = new Date();
  d.setTime(d.getTime() + (expires * 24*60*60*1000));
  document.cookie = key + "=" + value + ";expires=" + d.toUTCString();
}
