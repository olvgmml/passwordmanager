<div class="container">
  <div class="row justify-content-center align-items-center" style="height:75vh;">
    <form>
      <div class="card">
        <div class="card-body">
          <div class="form-group">
            <img src="images/olvg.png" class="ml-4">
          </div>
            <div class="form-group">
              <input type="text" class="form-control" id="username" placeholder="Gebruikersnaam" required>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="password" placeholder="Wachtwoord" required>
            </div>

            <div class="form-group">
              <div id="login_status" class="alert alert-danger" style="display: none;"></div>
            </div>

            <button type="submit" id="login" class="btn btn-primary">Inloggen</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script>
$(function() {
  $('#login').click(function(e) {
    e.preventDefault();

    $.post('login.php', {
      'username': $('#username').val(),
      'password': $('#password').val()
    }, function(data) {
      if (data.success) {
        window.location = '.';
      } else {
        $('#login_status').text(data.message).slideDown("fast");
      }
    });
  });
});
</script>
