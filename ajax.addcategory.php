<?php
  include('inc.global.php');

  if (!$logged_in) exit();

  if ($currentuser->role < 2) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("INSERT INTO `addcategorylog`(
    `category`,
    `candue`,
    `createdby`,
    `creationdate`
  ) VALUES(
    :category,
    :candue,
    :createdby,
    NOW()
  )");

  $stmt->execute([
    ':category' => $_POST['add-name'],
    ':candue' => $_POST['add-candue'],
    ':createdby' => $currentuser->id
  ]);

  $stmt = $db->prepare("INSERT INTO `categories` (
    `name`,
    `showdue`,
    `lastupdated`,
    `lastupdatedby`
  ) VALUES (
    :name,
    :showdue,
    NOW(),
    :lastupdatedby
  )");
  $stmt->bindParam(':name', $_POST['add-name']);
  $stmt->bindParam(':showdue', $_POST['add-candue']);
  $stmt->bindParam(':lastupdatedby', $currentuser->id);
  $stmt->execute();

  $db->commit();
?>
