<?php if ($currentuser->role == 3) { ?>
<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <h3>Edit Account Log</h3>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
  </div>
</div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Account description</th>
          <th scope="col">Old Username</th>
          <th scope="col">New Username</th>
          <th scope="col">Old Password</th>
          <th scope="col">New Password</th>
          <th scope="col">Edit User</th>
          <th scope="col">Edit Date</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            pl.`id`,
            pl.`account`,
            ac.`id`,
            ac.`username` as `acname`,
            pl.`oldusername`,
            pl.`newusername`,
            pl.`oldpassword`,
            pl.`newpassword`,
            pl.`edituser`,
            us.`name` as `usname`,
            DATE_FORMAT(pl.`editdate`, '%d-%m-%Y %H:%i') as `editdate`
          FROM `passwordlog` pl
          JOIN `accounts` ac ON pl.`account` = ac.`id`
          JOIN `users` us ON pl.`edituser` = us.`id`
          ORDER BY `editdate` ASC
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->acname?></td>
            <td><?=$row->oldusername?></td>
            <td><?=$row->newusername?></td>
            <td><?=$row->oldpassword?></td>
            <td><?=$row->newpassword?></td>
            <td><?=$row->usname?></td>
            <td><?=$row->editdate?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php } ?>
