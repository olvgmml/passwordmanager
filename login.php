<?php
  include('inc.global.php');

  if (!isset($_POST['username']) || !isset($_POST['password'])) exit();

  $retval = new stdClass();
  $retval->success = false;
  $retval->message = '';

  $ret = validateUser();

  if ($ret[0] === true) {
    $retval->success = true;
  } else {
    $retval->message = $ret[1];
  }

  header('Content-Type: application/json');
  print(json_encode($retval));
?>
