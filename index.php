<?php
  include('inc.global.php');

  if (isset($_GET['cat']) && is_int((int)$_GET['cat'])) {
    $cat = $_GET['cat'];
  } else {
    $cat = -1;
  }

  if (isset($_GET['a']) && $_GET['a'] != '') {
    $a = $_GET['a'];
  } else {
    $a = 'passwords';
  }

?>
<html>
<head>
  <title>OLVG Lab BV | PasswordManager</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/fontawesome.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/passwordmanager.js"></script>
  <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
</head>
<body>
  <nav class="navbar navbar-default navbar-expand-lg navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <?php if($logged_in){ ?>
          <a class="nav-link <?=($cat == -1) ? 'active' : '' ?>" href=".">Home</a>
        <?php } ?>
        </li>
        <?php
          $stmt = $db->prepare("SELECT
              c.`id`,
              c.`name`,
              c.`active`
            FROM `categories` c
            WHERE c.`active` = 1");
          $stmt->execute();
          while($row = $stmt->fetch()) {
            $active = ($row->id == $cat) ? 'active' : '';
            ?>
            <li class="nav-item">
              <?php if($logged_in){?>
              <a class="nav-link <?=$active?>" href="?cat=<?=$row->id?>"><?=$row->name?></a>
            <?php } else{  }?>
            </li>
            <?php
            }
            ?>
      </ul>

        <?php if ($logged_in) { ?>
        <label class="nav-link mt-2 color-light">Welkom: <?= $currentuser->name; ?> | <?= $currentuser->username?> </label>
        <?php if($currentuser->role != 3) {
          $dept = '';
          if (isset($_COOKIE['passwordmanager_dept'])) $dept = $_COOKIE['passwordmanager_dept'];
        ?>
        <select id="location" class="col-sm-1 mr-2 form-control">
          <?php
            $stmt = $db->prepare("SELECT
                ud.`id`,
                ud.`user`,
                ud.`department`,
                l.`name`
              FROM `userdepartments` ud
              JOIN `location` l ON ud.`department` = l.`id`
              WHERE ud.`user`=:id
              ");

            $stmt->execute([':id' => $currentuser->id]);
            while($row2 = $stmt->fetch()) {
              ?>
              <option value="<?=$row2->department?>" <?=($dept == $row2->department) ? 'selected' : ''?>><?=$row2->name?></option>
              <?php
              }
              ?>
        </select>
      <?php }?>
        <a class="link-light a mr-4" id="help" href="?a=help"><i class="far fa-question-circle"></i></a>
        <?php
          if ($logged_in && $currentuser->role == 3) {
         ?>

         <div class="dropdown">
          <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
            <i class="fas fa-cogs"></i> Manage
          </button>

          <div class="dropdown-menu">
            <a class="dropdown-item" href="?a=users"><i class="fas fa-users-cog"></i> User Management</a>
            <a class="dropdown-item" href="?a=categories"><i class="fas fa-users-cog"></i> Categories</a>
            <a class="dropdown-item" href="?a=locations"><i class="fas fa-users-cog"></i> Locations</a>
          </div>
        </div>
      <?php } if ($logged_in && $currentuser->username == 215501 && $currentuser->role == 3){?>
        <div class="dropdown">
         <button class="btn btn-light dropdown-toggle ml-2" type="button" id="dropdownLogButton" data-toggle="dropdown">
           <i class="fas fa-cogs"></i> Logging
         </button>
         <div class="dropdown-menu">
           <a class="dropdown-item" href="?a=editcategorylog"><i class="fas fa-users-cog"></i> Logging Edit Category</a>
           <a class="dropdown-item" href="?a=addcategorylog"><i class="fas fa-users-cog"></i> Logging Add Category</a>
           <a class="dropdown-item" href="?a=editaccountlog"><i class="fas fa-edit"></i> Logging Edit Account</a>
           <a class="dropdown-item" href="?a=addaccountlog"><i class="fas fa-plus-square"></i> Logging Add Account</a>
           <a class="dropdown-item" href="?a=edituserlog"><i class="fas fa-user-edit"></i> Logging Edit User</a>
           <a class="dropdown-item" href="?a=adduserlog"><i class="fas fa-user-plus"></i> Logging Add User</a>
         </div>
        </div>
        <?php
          }
        ?>
        <a class="btn btn-light ml-2" href="#" id="logout_button"><i class="fas fa-sign-out-alt"></i> Logout</a>
        <?php } ?>
        <script>
          $(function() {

            $('#location').change(function(){
              location.reload();
            });

            $("#search").on("keyup", function() {
              var value = $(this).val().toLowerCase();

              $('#clear').toggleClass('clear-hidden', value == '');

              $("#searchtable tbody tr").each(function(i, e) {
                var $e = $(e);
                $e.toggle($e.text().toLowerCase().indexOf(value) > -1)
              });
            });

            $('#clear').click(function(e) {
              e.preventDefault();
              $('#search').val('');
              $('#search').keyup();
            });

            $('#logout_button').click(function(){
              $.post('logout.php',  {}, function() { window.location = '.';});
            });

            $('#location').change(function(){
              setCookie('passwordmanager_dept', $(this).val(), 30);
            });
          });

        </script>

    </div>
  </nav>
  <div class="content">
  <?php
  if($logged_in) {
    if ($a == 'passwords'){
      include('module.passwords.php');
    }
    else if ($a == 'addnew') {
      include('module.addnew.php');
    }
    else if($a == 'adduser'){
      include('module.adduser.php');
    }
    else if($a == 'users'){
      include('module.users.php');
    }
    else if($a == 'help'){
      include('module.help.php');
    }
    else if($a == 'editaccountlog'){
      include('module.passwordlog.php');
    }
    else if($a == 'addaccountlog'){
      include('module.addaccountlog.php');
    }
    else if($a == 'edituserlog'){
      include('module.edituserlog.php');
    }
    else if($a == 'adduserlog'){
      include('module.adduserlog.php');
    }
    else if($a == 'categories'){
      include('module.categories.php');
    }
    else if($a == 'addcategorylog'){
      include('module.addcategorylog.php');
    }
    else if($a == 'editcategorylog'){
      include('module.editcategorylog.php');
    }
    else if($a == 'locations'){
      include('module.locations.php');
    }
  } else {
    include('module.login.php');
  } ?>
  </div>
</body>
</html>
