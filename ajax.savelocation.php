<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();


  $stmt = $db->prepare("SELECT
      `name`,
      `active`
    FROM `location`
    WHERE `id` = :id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->execute();

  $stmt = $db->prepare("UPDATE
      `location`
    SET
      `name` = :name,
      `lastupdated` = NOW(),
      `lastupdatedby` = $currentuser->id
    WHERE
      `id`=:id LIMIT 1");

  $stmt->bindParam(':id', $_POST['id']);
  $stmt->bindParam(':name', $_POST['save-location-name']);

  $stmt->execute();

?>
