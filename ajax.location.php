<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("SELECT
      l.`id`,
      l.`name`,
      l.`active`
    FROM
      `location` l
    WHERE
      `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->fetch();
  $stmt->execute();

  if ($row = $stmt->fetch()) {
    ?>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Wijzig: "<?= $row->name?>"</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="location-form" data-id="<?=$row->id?>">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Naam:</label>
            <input type="text" class="form-control" id="save-location-name" value="<?=$row->name?>" required>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Actief</label>
            <input type="text" class="form-control" id="save-location-active" value="<?php if($row->active == 1){ print('Actief');}else{print('Inactief');}?>" readonly>
          </div>

        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button id="pwdEdit-close" type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <?php
        if($currentuser->role == 3){
          if($row->active == 0){?>
            <button id="pwdEdit-activate" type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-user-plus"></i> Activeer</button>
          <?php } else{ ?>
            <button id="pwdEdit-delete" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-user-minus"></i> Deactiveer</button>
        <?php } }?>
        <button id="pwdEdit-save" type="submit" class="btn btn-primary">Opslaan</button>
      </div>

      <script>
      'use strict';

      $('#pwdEdit-save').click(function(e){
        e.preventDefault();

        $.post('ajax.savelocation.php', {
          'id': $('#location-form').data('id'),
          'save-location-name': $('#save-location-name').val()
        }, function() {
          $('#pwdEdit').modal('toggle');
            location.reload();
        });
      });

      $('#pwdEdit-activate').click(function(e){
        e.preventDefault();

        $.post('ajax.activatelocation.php',{
          'id': $('#location-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });

      $('#pwdEdit-delete').click(function(e){
        e.preventDefault();

        $.post('ajax.deactivatelocation.php',{
          'id': $('#location-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });
      </script>
    <?php
  }
?>
