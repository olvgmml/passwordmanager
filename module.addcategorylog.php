<?php if ($currentuser->role == 3) { ?>
<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <h3>Add Category Log</h3>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
  </div>
</div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Categorie</th>
          <th scope="col">Kan Verlopen</th>
          <th scope="col">Actief</th>
          <th scope="col">Aangemaakt Door</th>
          <th scope="col">Aangemaakt Op</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            acl.`id`,
            acl.`category`,
            acl.`candue`,
            sd.`candue` as `sname`,
            acl.`active`,
            acl.`createdby`,
            u.`name` as `uname`,
            DATE_FORMAT(acl.`creationdate`, '%d-%m-%Y %H:%i') as `creationdate`
          FROM `addcategorylog` acl
          JOIN `users` u ON acl.`createdby` = u.`id`
          JOIN `showdue` sd ON acl.`candue` = sd.`id`
          ORDER BY `creationdate` ASC
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->category?></td>
            <td><i class="active-check far <?=($row->sname == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><i class="active-check far <?=($row->active == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><?=$row->uname?></td>
            <td><?=$row->creationdate?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php } ?>
