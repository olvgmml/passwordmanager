<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("SELECT `username`, `password` FROM `accounts` WHERE `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->execute();
  $oldrow = $stmt->fetch();

  $stmt = $db->prepare("INSERT INTO `passwordlog` (
    `account`,
    `oldusername`,
    `newusername`,
    `oldpassword`,
    `newpassword`,
    `edituser`,
    `editdate`
  ) VALUES (
    :id,
    :oldusername,
    :newusername,
    :oldpassword,
    :newpassword,
    :edituser,
    NOW()
  )");

  $stmt->execute([
    ':id' => $_POST['id'],
    ':oldusername' => $oldrow->username,
    ':newusername' => $_POST['edit-username'],
    ':oldpassword' => $oldrow->password,
    ':newpassword' => $_POST['edit-password'],
    ':edituser' => $currentuser->id
  ]);

  $stmt = $db->prepare("UPDATE
      `accounts`
    SET
      `name` = :name,
      `username` = :username,
      `password` = :password,
      `category` = :category,
      `location` = :location,
      `lastupdated` = NOW(),
      `lastupdatedby` = $currentuser->id
    WHERE
      `id`=:id LIMIT 1");

  $stmt->bindParam(':id', $_POST['id']);
  $stmt->bindParam(':name', $_POST['name']);
  $stmt->bindParam(':username', $_POST['edit-username']);
  $stmt->bindParam(':password', $_POST['edit-password']);
  $stmt->bindParam(':category', $_POST['edit-cat']);
  $stmt->bindParam(':location', $_POST['edit-location']);
  
  $stmt->execute();

  $db->commit();
?>
