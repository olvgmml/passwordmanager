<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("SELECT `password` FROM `accounts` WHERE `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->execute();

  if($row = $stmt->fetch()) {
    print($row->password);
  }
?>
