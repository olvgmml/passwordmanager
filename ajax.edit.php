<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("SELECT
      a.`id`,
      a.`name`,
      a.`username`,
      a.`password`,
      a.`active`,
      a.`category`,
      a.`location`,
      a.`lastupdated`,
      a.`lastupdatedby`
    FROM
      `accounts` a
    WHERE
      `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->fetch();
  $stmt->execute();

  if ($row = $stmt->fetch()) {
    ?>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Wijzig: "<?= $row->name?>"</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="edit-form" data-id="<?=$row->id?>">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Omschrijving:</label>
            <input type="text" class="form-control" id="edit-name" value="<?=$row->name?>" required>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Gebruikersnaam:</label>
            <?php if($currentuser->role == 3){ ?>
              <input type="text" class="form-control" id="edit-username" value="<?=$row->username?>" required>
            <?php } else if($currentuser->role ==2){ ?>
              <input type="text" class="form-control" id="edit-username" value="<?=$row->username?>" readonly>
            <?php } ?>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Wachtwoord:</label>
            <input type="text" class="form-control" id="edit-password" value="<?=$row->password?>" required>
          </div>
          <?php if($currentuser->role >= 2){ ?>

            <div class="form-group">
              <label for="edit-cat" class="col-form-label">Categorie:</label>
              <select id="edit-cat" class="form-control" required>
                <?php
                  $stmt = $db->prepare("SELECT `id`, `name` FROM `categories` c");
                  $stmt->execute();
                  while($row1 = $stmt->fetch()) {
                    ?>
                    <option value="<?=$row1->id?>" <?=($row->category == $row1->id) ? ' selected' : ''?>><?=$row1->name?></option>
                    <?php
                  }
                    ?>
              </select>
            </div>

            <div class="form-group">
              <label for="edit-location" class="col-form-label">Locatie:</label>
              <?php if($currentuser->role == 3){ ?>
                <select id="edit-location" class="form-control">
              <?php } else if($currentuser->role == 2){ ?>
                <select id="edit-location" class="form-control" readonly disabled>
              <?php } ?>
                <?php
                  $stmt = $db->prepare("SELECT `id`, `name` FROM `location` l");
                  $stmt->execute();
                  while($row2 = $stmt->fetch()) {
                    ?>
                    <option value="<?=$row2->id?>" <?=($row->location == $row2->id) ? ' selected' : ''?>><?=$row2->name?></option>
                    <?php
                  }
                    ?>
              </select>
            </div>

            <div class="form-group">
              <label for="message-text" class="col-form-label">Actief</label>
              <input type="text" class="form-control" id="edit-active" value="<?php if($row->active == 1){ print('Actief');}else{print('Inactief');}?>" readonly>
            </div>
          <?php } ?>
        </form>
      </div>

      <div class="modal-footer justify-content-between">
        <button id="pwdEdit-close" type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <?php
        if($currentuser->role == 3){
          if($row->active == 0){?>
            <button id="pwdEdit-activate" type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-user-plus"></i> Activeer</button>
          <?php } else{ ?>
            <button id="pwdEdit-delete" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-user-minus"></i> Deactiveer</button>
        <?php } }?>
        <button id="pwdEdit-save" type="submit" class="btn btn-primary">Opslaan</button>
      </div>

      <script>
      'use strict';

      $('#pwdEdit-save').click(function(e){
        e.preventDefault();

        $.post('ajax.savepassword.php', {
          'id': $('#edit-form').data('id'),
          'name': $('#edit-name').val(),
          'edit-username': $('#edit-username').val(),
          'edit-password':$('#edit-password').val(),
          'edit-cat': $('#edit-cat').val(),
          'edit-location': $('#edit-location').val(),
        }, function(){
          $('#pwdEdit').modal('toggle');
            location.reload();
        });
      });

      $('#pwdEdit-delete').click(function(e){
        e.preventDefault();
        $.post('ajax.deleteaccount.php',{
          'id': $('#edit-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });

      $('#pwdEdit-activate').click(function(e){
        e.preventDefault();
        $.post('ajax.activateaccount.php',{
          'id': $('#edit-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });
      </script>
    <?php
  }
?>
