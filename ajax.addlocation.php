<?php
  include('inc.global.php');

  if (!$logged_in) exit();

  if ($currentuser->role < 3) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("INSERT INTO `location` (
    `name`,
    `lastupdated`,
    `lastupdatedby`
  ) VALUES (
    :name,
    NOW(),
    :lastupdatedby
  )");
  $stmt->bindParam(':name', $_POST['location-name']);
  $stmt->bindParam(':lastupdatedby', $currentuser->id);
  $stmt->execute();

  $db->commit();
?>
