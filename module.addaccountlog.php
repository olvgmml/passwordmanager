<?php if ($currentuser->role == 3) { ?>
<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <h3>Add Account Log</h3>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
  </div>
</div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Username</th>
          <th scope="col">Password</th>
          <th scope="col">Omschrijving</th>
          <th scope="col">Locatie</th>
          <th scope="col">Categorie</th>
          <th scope="col">Aangemaakt door</th>
          <th scope="col">Aangemaakt op</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            aal.`id`,
            aal.`newusername`,
            aal.`newpassword`,
            aal.`description`,
            aal.`location`,
            l.`name` as `locname`,
            aal.`category`,
            c.`name` as `catname`,
            aal.`user`,
            u.`name` as `uname`,
            DATE_FORMAT(aal.`creationdate`, '%d-%m-%Y %H:%i') as `creationdate`
          FROM `addaccountlog` aal
          JOIN `location` l ON aal.`location` = l.`id`
          JOIN `categories` c ON aal.`category` = c.`id`
          JOIN `users` u ON aal.`user` = u.`id`
          ORDER BY `creationdate` ASC
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->newusername?></td>
            <td><?=$row->newpassword?></td>
            <td><?=$row->description?></td>
            <td><?=$row->locname?></td>
            <td><?=$row->catname?></td>
            <td><?=$row->uname?></td>
            <td><?=$row->creationdate?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php } ?>
