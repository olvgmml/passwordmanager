<?php if ($currentuser->role == 3) { ?>
  <div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <?php if($currentuser->role >= 2) { ?>
            <button id="adduserbtn" class="btn btn-primary"><i class="fas fa-plus-square"></i> Nieuwe Categorie</button>
          <?php } ?>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>

    <div class="card mb-2" id="newuserform" style="display: none;">
      <div class="card-body">
        <form id="addpassform">
          <div class="form-group row">
            <label for="form-password" class="col-sm-2 col-form-label">Naam</label>
            <input type="text" class="col-sm-6 form-control" id="add-name" placeholder="Naam" required>
          </div>

          <div class="form-group row">
            <label for="form-candue" class="col-sm-2 col-form-label">Kan verlopen</label>
            <select id="add-candue" class="col-sm-6 form-control" required>
              <option></option>
              <?php
                $stmt = $db->prepare("SELECT `id`, `name` FROM `showdue` cd");
                $stmt->execute();
                while($row = $stmt->fetch()) {
                  ?>
                  <option value="<?=$row->id?>"><?=$row->name?></option>
                  <?php
                }
                  ?>
            </select>
          </div>

          <div class="form-group row">
            <div class="col-4"><button class="btn btn-secondary" id="button-cancel" type="reset"><i class="fas fa-times"></i> Annuleren</button></div>
            <div class="col-4 pr-0"><button class="btn btn-primary float-right" id="button-save" type="submit"><i class="fas fa-check"></i> Opslaan</button></div>
          </div>
        </form>
      </div>
    </div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Laatst Bijgewerkt</th>
          <th scope="col">Bijgewerkt Door</th>
          <th scope="col">Active</th>
          <th scope="col">Kan Verlopen</th>
          <th scope="col">Edit</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            c.`id`,
            c.`name`,
            DATE_FORMAT(c.`lastupdated`, '%d-%m-%Y %H:%i') as `lastupdated`,
            c.`lastupdatedby`,
            c.`active`,
            c.`showdue`,
            u.`name` as `uname`
          FROM `categories` c
          LEFT JOIN `users` u ON c.`lastupdatedby` = u.`id`
          ORDER BY c.`active` DESC
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->name?></td>
            <td><?=$row->lastupdated?></td>
            <td><?=$row->uname?></td>
            <td><i class="active-check far <?=($row->active == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><i class="active-check far <?=($row->showdue == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><a href="#" class="edit-link"><i class="fas fa-edit"></i></a></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script>
'use strict';
  $(function() {

    $('#adduserbtn').click(function(e) {
      if($(this).hasClass('clicked')){
        $('#newuserform').slideUp('fast');
        $('#adduserbtn').removeClass('clicked');
        $('#add-username').trigger(':reset');
      }else{
        $('#newuserform').slideDown('fast');
        $('#adduserbtn').addClass('clicked');
      }
    });

    $('#addpassform').submit(function(e){
      e.preventDefault();

      $.post('ajax.addcategory.php', {
        'add-name': $('#add-name').val(),
        'add-candue': $('#add-candue').val()
      }, function(){
        window.location.href = "./?a=categories";
      });
    });

    $('#button-cancel').click(function(e){
      if($('#adduserbtn').hasClass('clicked')){
        $('#adduserbtn').removeClass('clicked');
      }
      $('#newuserform').slideUp('fast');
    });

    $('.edit-link').click(function(e){
      e.preventDefault();

      var id = $(this).parent().parent().data('id');

      $('#pwdEdit .modal-content').load('ajax.category.php', {
        'id': id
      });

      $('#pwdEdit').modal({});
    });

    $('#pwdEdit').on('hidden.bs.modal', function(e) {
      $('#pwdEdit .modal-content').html('<div class="loader-holder"><img src="images/loading.gif" class="loader"></div>');
    });
  });
</script>

<div class="modal fade" id="pwdEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="loader-holder"><img src="images/loading.gif" class="loader"></div>
    </div>
  </div>
</div>
<?php } ?>
