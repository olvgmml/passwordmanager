<?php if ($currentuser->role == 3) { ?>
<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <h3>Edit User Log</h3>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
  </div>
</div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Old Username</th>
          <th scope="col">New Username</th>
          <th scope="col">Old Name</th>
          <th scope="col">New Name</th>
          <th scope="col">Old Role</th>
          <th scope="col">New Role</th>
          <th scope="col">Old Location</th>
          <th scope="col">New Location</th>
          <th scope="col">Edit User</th>
          <th scope="col">Edit Date</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            el.`oldusername`,
            el.`newusername`,
            el.`oldname`,
            el.`newname`,
            el.`oldrole`,
            our.`name` as `orole`,
            el.`newrole`,
            nur.`name` as `nrole`,
            el.`oldlocation`,
            ol.`name` as `oloc`,
            el.`newlocation`,
            nl.`name` as `nloc`,
            el.`edituser`,
            u.`name` as `uname`,
            DATE_FORMAT(el.`editdate`, '%d-%m-%Y %H:%i') as `editdate`
          FROM `edituserlog` el
          JOIN `location` ol ON el.`oldlocation` = ol.`id`
          JOIN `location` nl ON el.`newlocation` = nl.`id`
          JOIN `users` u ON el.`edituser` = u.`id`
          JOIN `userroles` our ON el.`oldrole` = our.`id`
          JOIN `userroles` nur ON el.`newrole` = nur.`id`
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->oldusername?></td>
            <td><?=$row->newusername?></td>
            <td><?=$row->oldname?></td>
            <td><?=$row->newname?></td>
            <td><?=$row->orole?></td>
            <td><?=$row->nrole?></td>
            <td><?=$row->oloc?></td>
            <td><?=$row->nloc?></td>
            <td><?=$row->uname?></td>
            <td><?=$row->editdate?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php } ?>
