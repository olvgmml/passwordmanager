<?php if ($currentuser->role == 3) { ?>
  <div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <?php if($currentuser->role >= 2) { ?>
            <button id="adduserbtn" class="btn btn-primary"><i class="fas fa-plus-square"></i> Nieuwe Gebruiker</button>
          <?php } ?>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
    <div class="card mb-2" id="newuserform" style="display: none;">
      <div class="card-body">
        <form id="addpassform">
          <div class="form-group row">
            <label for="add-role" class="col-sm-2 col-form-label">Rechten</label>
            <select id="add-role" name="add-role" class="col-sm-6 form-control" placeholder="Rechten" required>
              <option></option>
              <?php
                $stmt = $db->prepare("SELECT `id`, `name` FROM `userroles` ur");
                $stmt->execute();
                while($row = $stmt->fetch()) {
                  ?>
                  <option value="<?=$row->id?>"><?=$row->name?></option>
                  <?php
                }
                  ?>
            </select>
          </div>

          <div class="form-group row">
            <label for="form-username" class="col-sm-2 col-form-label">Username</label>
            <input type="text" class="col-sm-6 form-control" id="add-username" name="add-username" placeholder="Username" required>
          </div>

          <div class="form-group row">
            <label for="form-password" class="col-sm-2 col-form-label">Naam</label>
            <input type="text" class="col-sm-6 form-control" id="add-name" name="add-name" placeholder="Naam" required>
          </div>

          <div class="form-group row">
            <label for="add-location" class="col-sm-2 col-form-label">Locatie</label>
              <div class="form-check">
                <?php
                  $stmt = $db->prepare("SELECT
                                          l.`id`,
                                          l.`name`,
                                          l.`active`
                                        FROM `location` l
                                        WHERE l.`active` = 1
                                        ORDER BY l.`name` ASC");
                  $stmt->execute();
                  while($row = $stmt->fetch()) {
                    ?>
                    <div>
                      <input id="check-<?=$row->id?>" class="form-check-input" type="checkbox" name="add-locations[]" value="<?=$row->id?>">
                      <label class="form-check-label" for="check-<?=$row->id?>"> <?=$row->name?></label>
                    </div>
                    <?php
                  }
                    ?>
              </div>
          </div>

          <div class="form-group row">
            <div class="col-4"><button class="btn btn-secondary" id="button-cancel" type="reset"><i class="fas fa-times"></i> Annuleren</button></div>
            <div class="col-4 pr-0"><button class="btn btn-primary float-right" id="button-save" type="submit"><i class="fas fa-check"></i> Opslaan</button></div>
          </div>
        </form>
      </div>
    </div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Username</th>
          <th scope="col">Naam</th>
          <th scope="col">Rechten</th>
          <th scope="col">Aangemaakt Op</th>
          <th scope="col">Aangemaakt Door</th>
          <th scope="col">Laatsts Bijgewerkt</th>
          <th scope="col">Actief</th>
          <th scope="col">Edit</th>
        </tr>
      </thead>
      <tbody>
      <?php


        $stmt = $db->prepare("SELECT
            u.`id`,
            u.`username`,
            u.`role`,
            ur.`name` as `rolename`,
            u2.`name` as `createdby`,
            DATE_FORMAT(u.`creationdate`, '%d-%m-%Y %H:%i') as `creationdate`,
            u.`name`,
            u.`active`,
            DATE_FORMAT(u.`lastupdated`, '%d-%m-%Y %H:%i') as `lastupdated`
          FROM `users` u
          INNER JOIN `users` u2 ON u2.`id` = u.`createdby`
          JOIN `userroles` ur ON u.`role` = ur.`id`
          ORDER BY `active` DESC, `username` ASC
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->username?></td>
            <td><?=$row->name?></td>
            <td><?=$row->rolename?></td>
            <td><?=$row->creationdate?></td>
            <td><?=$row->createdby?></td>
            <td><?=$row->lastupdated?></td>
            <td><i class="active-check far <?=($row->active == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><a href="#" class="edit-link"><i class="fas fa-edit"></i></a></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script>
'use strict';
  $(function() {

    $('#adduserbtn').click(function(e) {
      if($(this).hasClass('clicked')){
        $('#newuserform').slideUp('fast');
        $('#adduserbtn').removeClass('clicked');
        $('#add-username').trigger(':reset');
      }else{
      $('#newuserform').slideDown('fast');
      $('#adduserbtn').addClass('clicked');
    }
    });

    $('#addpassform').submit(function(e){
              e.preventDefault();

              /*$.post('ajax.adduser.php', {
                'add-role': $('#add-role').val(),
                'add-username': $('#add-username').val(),
                'add-location': $('#add-location').val(),
                'add-name': $('#add-name').val()
              }, function(){
                window.location.href = "?a=users";
              });*/

              $.post('ajax.adduser.php', $('#addpassform').serialize(), function(){
                window.location.href = "?a=users";
              });
            });

    $('#button-cancel').click(function(e){
      if($('#adduserbtn').hasClass('clicked')){
        $('#adduserbtn').removeClass('clicked');
      }
      $('#newuserform').slideUp('fast');
    });

    $('.edit-link').click(function(e){
      e.preventDefault();

      var id = $(this).parent().parent().data('id');
      console.log('id: ' + id);

      $('#pwdEdit .modal-content').load('ajax.user.php', {
        'id': id
      });

      $('#pwdEdit').modal({});
    });

    $('#pwdEdit').on('hidden.bs.modal', function(e) {
      $('#pwdEdit .modal-content').html('<div class="loader-holder"><img src="images/loading.gif" class="loader"></div>');
    });


  });
</script>

<div class="modal fade" id="pwdEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="loader-holder"><img src="images/loading.gif" class="loader"></div>
    </div>
  </div>
</div>
<?php } ?>
