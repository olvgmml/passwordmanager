<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("UPDATE
      `categories`
    SET
      `showdue` = '0',
      `lastupdated` = NOW(),
      `lastupdatedby` = $currentuser->id
    WHERE
      `id`=:id LIMIT 1");
      $stmt->bindParam(':id', $_POST['id']);
      $stmt->execute();
?>
