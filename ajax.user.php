<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("SELECT
      u.`id`,
      u.`username`,
      u.`role`,
      u.`name`,
      u.`active`,
      u.`createdby`
    FROM
      `users` u
    WHERE
      `id`=:id LIMIT 1");
  $stmt->execute([':id' => $_POST['id']]);

  if ($row = $stmt->fetch()) {
    ?>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Wijzig: "<?=$row->name?>"</h5>
        <button type="button" class="close" data-dismiss="modal">X</button>
      </div>
      <div class="modal-body">
        <form id="users-form" data-id="<?=$row->id?>">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Gebruikersnaam:</label>
            <input type="text" class="form-control" id="users-username" value="<?=$row->username?>" required>
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Naam:</label>
            <input type="text" class="form-control" id="users-name" value="<?=$row->name?>">
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Rechten:</label>
              <select class="form-control" id="users-role">
              <?php
                $stmt = $db->prepare("SELECT `id`, `name` FROM `userroles`");
                $stmt->execute();
                while($row2 = $stmt->fetch()) { ?>
                  <option value="<?=$row2->id?>" <?=($row->role == $row2->id) ? ' selected' : ''?>><?=$row2->name?></option>
                <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Locatie:</label>
            <div class="form-check">
              <?php
                $stmt = $db->prepare("SELECT
                                        l.`id`,
                                        l.`name`,
                                        IF(ud.`id` IS NULL, 0, 1) as `selected`
                                      FROM `location` l
                                      LEFT JOIN `userdepartments` ud ON ud.`department` = l.`id` AND ud.`user` = :user
                                      ORDER BY l.`name` ASC");
                $stmt->execute([':user' => $_POST['id']]);
                while($row2 = $stmt->fetch()) {
                  ?>
                  <div>
                    <input id="check-<?=$row2->id?>" class="form-check-input" name="users-locations[]" type="checkbox" value="<?=$row2->id?>" <?=$row2->selected ? 'checked' : ''?>>
                    <label class="form-check-label" for="check-<?=$row2->id?>"> <?=$row2->name?> </label>
                  </div>
                  <?php
                }
                  ?>
            </div>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Actief</label>
            <input type="text" class="form-control" id="edit-active" value="<?php if($row->active == 1){ print('Actief');}else{print('Inactief');}?>" readonly>
          </div>
        </form>
      </div>

      <div class="modal-footer justify-content-between">
        <button id="pwdEdit-close" type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Sluiten</button>
        <?php
        if($row->active == 0){

          ?><button id="pwdEdit-activate" type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-user-plus"></i> Activeer</button>
        <?php } else{ ?>
        <button id="pwdEdit-delete" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-user-minus"></i> Deactiveer</button>
      <?php }?>
        <button id="pwdEdit-save" type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Opslaan</button>
      </div>

      <script>
      'use strict';

        $('#pwdEdit-save').click(function(e){
          e.preventDefault();

          $.post('ajax.saveusers.php', {
            'id': $('#users-form').data('id'),
            'users-username':$('#users-username').val(),
            'users-role': $('#users-role').val(),
            'users-name': $('#users-name').val(),
            'users-location': $('#users-location').val(),
            'users-active': $('#users-active').val()
          }, function(){
            $('#pwdEdit').modal('toggle');
            location.reload();
          });
        });

      $('#pwdEdit-delete').click(function(e){
        e.preventDefault();
        $.post('ajax.deleteusers.php',{
          'id': $('#users-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });

      $('#pwdEdit-activate').click(function(e){
        e.preventDefault();
        $.post('ajax.activateusers.php',{
          'id': $('#users-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });
      </script>
    <?php
  }
?>
