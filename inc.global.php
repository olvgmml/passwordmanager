<?php

  session_start();

  $config = new stdClass();
  $config->ldapserver = "ldap://pait01.olvg.nl:389 ldap://pait02.olvg.nl:389";
  $config->ldapdomain = "OLVG-NET";
  $config->dbhost = "olvg-21077";
  $config->dbname = "password_manager";
  $config->dbuser = "password_manager";
  $config->dbpass = "da9a0259e9e26a0b2d1dea871a573485";

  $db = new PDO("mysql:host={$config->dbhost};dbname={$config->dbname}", $config->dbuser, $config->dbpass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ]);

  $expireAfter = 60;

  function validateLDAP($username, $password, $server, $domain) {
    if (!$ldapconn = ldap_connect($server)) {
      return false;
    } else {
      $ldapbind = @ldap_bind($ldapconn, $domain . "\\" . $username, $password);

      return ($ldapbind && $password != null);
    }
  }

  function validateUser() {
    global $db, $config;

    if ((!isset($_SESSION['username']) || !isset($_SESSION['password'])) && (!isset($_POST['username']) || !isset($_POST['password']))) {
      return array(false, 'Onbekende gebruiker', null);
    } else {
      if (isset($_POST['username']) || isset($_POST['password'])) {
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['password'] = $_POST['password'];
      }

      $stmt = $db->prepare("SELECT * FROM `users` WHERE `username`=:username AND `active`=1 LIMIT 1");
      $stmt->bindParam(':username', $_SESSION['username']);
      $stmt->execute();
      if ($user = $stmt->fetch()) {
        if (validateLDAP($_SESSION['username'], $_SESSION['password'], $config->ldapserver, $config->ldapdomain)) {
          return array(true, '', $user);
        } else {
          return array(false, 'Verkeerd wachtwoord', null);
        }
      } else {
        return array(false, 'Onbekende gebruiker', null);
      }

      $stmt->close();
    }
  }

  $logged_in = false;
  $ret = validateUser();

  if ($ret[0] === true) {
    $logged_in = true;
    $currentuser = $ret[2];
  }
  ?>
