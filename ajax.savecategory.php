<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("SELECT `name`, `showdue`, `active` FROM `categories` WHERE `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->execute();
  $oldrow = $stmt->fetch();

  $stmt = $db->prepare("INSERT INTO `editcategorylog` (
    `oldcategoryname`,
    `newcategoryname`,
    `lastupdated`,
    `lastupdatedby`
  ) VALUES (
    :oldcategoryname,
    :newcategoryname,
    NOW(),
    :lastupdatedby
  )");

  $stmt->bindParam(':oldcategoryname', $oldrow->name);
  $stmt->bindParam(':newcategoryname', $_POST['cat-ncatname']);
  $stmt->bindParam(':lastupdatedby', $currentuser->id);

  $stmt->execute();

  $stmt = $db->prepare("UPDATE
      `categories`
    SET
      `name` = :name,
      `lastupdated` = NOW(),
      `lastupdatedby` = $currentuser->id
    WHERE
      `id`=:id LIMIT 1");

  $stmt->bindParam(':id', $_POST['id']);
  $stmt->bindParam(':name', $_POST['cat-ncatname']);
  
  $stmt->execute();

  $db->commit();
?>
