<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if ($currentuser->role < 2) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("INSERT INTO `addaccountlog`(
    `newusername`,
    `newpassword`,
    `description`,
    `location`,
    `category`,
    `user`,
    `creationdate`
  ) VALUES(
    :username,
    :passwords,
    :description,
    :locations,
    :categories,
    :user,
    NOW()
  )");

  $stmt->execute([
    ':username' => $_POST['form-username'],
    ':passwords' => $_POST['form-password'],
    ':description' => $_POST['form-name'],
    ':locations' => $_POST['form-location'],
    ':categories' => $_POST['form-category'],
    ':user' => $currentuser->id
  ]);

  $stmt = $db->prepare("INSERT INTO `accounts` (
    `name`,
    `username`,
    `password`,
    `category`,
    `location`,
    `lastupdated`,
    `lastupdatedby`
  ) VALUES (
    :name,
    :username,
    :password,
    :category,
    :location,
    NOW(),
    :lastupdatedby
  )");
  $stmt->bindParam(':name', $_POST['form-name']);
  $stmt->bindParam(':username', $_POST['form-username']);
  $stmt->bindParam(':password', $_POST['form-password']);
  $stmt->bindParam(':category', $_POST['form-category']);
  $stmt->bindParam(':location', $_POST['form-location']);
  $stmt->bindParam(':lastupdatedby', $currentuser->id);
  $stmt->execute();

  $db->commit();
?>
