<?php if ($currentuser->role == 3) { ?>
<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <h3>Edit Category Log</h3>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
  </div>
</div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Old Category Name</th>
          <th scope="col">New Category Name</th>
          <th scope="col">Last Updated</th>
          <th scope="col">Last Updated By</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            ecl.`oldcategoryname` as `oldname`,
            ecl.`newcategoryname` as `newname`,
            DATE_FORMAT(ecl.`lastupdated`, '%d-%m-%Y %H:%i') as 'lastupdated',
            ecl.`lastupdatedby`,
            u.`name` as `uname`
          FROM `editcategorylog` ecl
          LEFT JOIN  `users` u ON ecl.`lastupdatedby` = u.`id`
        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->oldname?></td>
            <td><?=$row->newname?></td>
            <td><?=$row->lastupdated?></td>
            <td><?=$row->uname?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php } ?>
