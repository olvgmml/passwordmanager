<?php
  include('inc.global.php');

  if (!$logged_in) exit();

  if (!isset($_POST['users-username']) || $_POST['users-username'] == '') exit();
  if (!isset($_POST['users-name']) || $_POST['users-name'] == '') exit();
  if (!isset($_POST['users-role']) || $_POST['users-role'] == '') exit();
  //if (!isset($_POST['add-locations']) || !is_array($_POST['add-locations']) || count($_POST['add-locations']) < 1) exit();

$db->beginTransaction();

  $stmt = $db->prepare("UPDATE
    `users`
  SET
    `username`=:username,
    `role`=:role,
    `name`=:name,
    `lastupdated` = NOW(),
    `lastupdatedby` = $currentuser->id
  WHERE `id`=:id LIMIT 1");

  $stmt->bindParam(':id', $_POST['id']);
  $stmt->bindParam(':username', $_POST['users-username']);
  $stmt->bindParam(':role', $_POST['users-role']);
  $stmt->bindParam(':name', $_POST['users-name']);
  $stmt->execute();

$db->commit();

?>
