<?php
  include('inc.global.php');

  if (!$logged_in) exit();

  if (!isset($_POST['add-username']) || $_POST['add-username'] == '') exit();
  if (!isset($_POST['add-name']) || $_POST['add-name'] == '') exit();
  if (!isset($_POST['add-role']) || $_POST['add-role'] == '') exit();
  if (!isset($_POST['add-locations']) || !is_array($_POST['add-locations']) || count($_POST['add-locations']) < 1) exit();

  $db->beginTransaction();

  $stmt = $db->prepare("INSERT INTO `users` (
    `username`,
    `role`,
    `name`,
    `createdby`,
    `creationdate`
  ) VALUES (
    :username,
    :role,
    :name,
    $currentuser->id,
    NOW()
  )");
  $stmt->bindParam(':username', $_POST['add-username']);
  $stmt->bindParam(':name', $_POST['add-name']);
  $stmt->bindParam(':role', $_POST['add-role']);
  $stmt->execute();

  $newuserid = $db->lastInsertId();

  $userloc = 0;
  $stmt = $db->prepare("INSERT INTO `userdepartments` (
    `user`,
    `department`
  ) VALUES (
    $newuserid,
    :department
  )");

  $stmt->bindParam(':department', $userloc);

  foreach($_POST['add-locations'] as $loc) {
    $userloc = (int)$loc;
    $stmt->execute();
  }

  $db->commit();

?>
