<?php if ($currentuser->role == 3) { ?>
  <div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <?php if($currentuser->role >= 2) { ?>
            <button id="addlocationbtn" class="btn btn-primary"><i class="fas fa-plus-square"></i> Nieuwe Locatie</button>
          <?php } ?>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
    <div class="card mb-2" id="addlocationform" style="display: none;">
      <div class="card-body">
        <form id="addpassform">
          <div class="form-group row">
            <label for="location-name" class="col-sm-2 col-form-label">Naam</label>
            <input type="text" class="col-sm-6 form-control" id="location-name" placeholder="Naam" required>
          </div>

          <div class="form-group row">
            <div class="col-4"><button class="btn btn-secondary" id="button-cancel" type="reset"><i class="fas fa-times"></i> Annuleren</button></div>
            <div class="col-4 pr-0"><button class="btn btn-primary float-right" id="button-save" type="submit"><i class="fas fa-check"></i> Opslaan</button></div>
          </div>
        </form>
      </div>
    </div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Lastupdated</th>
          <th scope="col">Laatst bijgewerkt</th>
          <th scope="col">Active</th>
          <th scope="col">Edit</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            l.`id`,
            l.`name`,
            l.`active`,
            DATE_FORMAT(l.`lastupdated`, '%d-%m-%Y %H:%i') as `lastupdated`,
            l.`lastupdatedby`,
            u.`name` as `uname`
          FROM
            `location` l
          LEFT JOIN `users` u ON l.`lastupdatedby` = u.`id`
          ORDER BY l.`active` DESC, l.`name` ASC
        ");
          $stmt->execute();

          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->name?></td>
            <td><?=$row->uname?></td>
            <td><?=$row->lastupdated?></td>
            <td><i class="active-check far <?=($row->active == 1) ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><a href="#" class="edit-link"><i class="fas fa-edit"></i></a></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script>
'use strict';
  $(function() {

    $('#addlocationbtn').click(function(e) {
      if($(this).hasClass('clicked')){
        //document.getElementById('')

        $('#addlocationform').slideUp('fast');
        $('#addlocationbtn').removeClass('clicked');
      }else{
        $('#addlocationform').slideDown('fast');
        $('#addlocationbtn').addClass('clicked');
      }
    });

    $('#addlocationform').submit(function(e){
              e.preventDefault();

              $.post('ajax.addlocation.php', {
                'location-name': $('#location-name').val()
              }, function(){
                window.location.href = "./?a=locations";
              });
            });

    $('#button-cancel').click(function(e){
      if($('#addlocationbtn').hasClass('clicked')){
        $('#addlocationbtn').removeClass('clicked');
      }
      $('#addlocationform').slideUp('fast');
    });

    $('.edit-link').click(function(e){
      e.preventDefault();

      var id = $(this).parent().parent().data('id');
      $('#pwdEdit .modal-content').load('ajax.location.php', {
        'id': id
      });

      $('#pwdEdit').modal({});
    });

    $('#pwdEdit').on('hidden.bs.modal', function(e) {
      $('#pwdEdit .modal-content').html('<div class="loader-holder"><img src="images/loading.gif" class="loader"></div>');
    });
  });
</script>

<div class="modal fade" id="pwdEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="loader-holder"><img src="images/loading.gif" class="loader"></div>
    </div>
  </div>
</div>
<?php } ?>
