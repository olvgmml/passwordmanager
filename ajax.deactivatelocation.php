<?php
include('inc.global.php');

if(!$logged_in) exit();
if($currentuser->role < 3) exit();
if(!isset($_POST['id'])) exit();

$stmt = $db->prepare("SELECT
    `name`,
    `active`
  FROM
    `location`
  WHERE `id` = :id LIMIT 1");
$stmt->bindParam(':id', $_POST['id']);
$stmt->execute();

$db->beginTransaction();

$stmt = $db->prepare("UPDATE
    `location`
  SET
    `active` = '0',
    `lastupdated` = NOW(),
    `lastupdatedby` = $currentuser->id
  WHERE
    `id` = :id LIMIT 1
  ");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->execute();

$db->commit();
 ?>
