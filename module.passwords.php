<div class="wrapper">
  <div class="container-fluid my-3">
    <nav class="navbar navbar-light bg-light">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <?php if($currentuser->role >= 2) { ?>
            <button id="addpassbtn" class="btn btn-primary"><i class="fas fa-plus-square"></i> Toevoegen</button>
          <?php } ?>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
        <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
      </form>
    </nav>
    <div class="card mb-2" id="newpassform" style="display: none;">
      <div class="card-body">
        <form id="addpassform">
          <div class="form-group row">
            <label for="form-cat" class="col-sm-2 col-form-label">Categorie:</label>
            <select id="form-cat" class="col-sm-6 form-control" required>
              <option></option>
              <?php
                $stmt = $db->prepare("SELECT `id`, `name`, `active` FROM `categories` c WHERE `active` = 1");
                $stmt->execute();
                while($row = $stmt->fetch()) {
                  ?>
                  <option value="<?=$row->id?>"><?=$row->name?></option>
                  <?php
                }
                  ?>
            </select>
          </div>

          <div class="form-group row">
            <label for="form-location" class="col-sm-2 col-form-label">Locatie:</label>
            <select id="form-location" class="col-sm-6 form-control" required>
              <?php
                $stmt = $db->prepare("SELECT `id`, `name`, `active` FROM `location` l WHERE `active` = 1");
                $stmt->execute();
                while($row2 = $stmt->fetch()) {
                  ?>
                  <option value="<?=$row2->id?>"><?=$row2->name?></option>
                  <?php
                }
                  ?>
            </select>
          </div>

          <div class="form-group row">
            <label for="form-name" class="col-sm-2 col-form-label">Omschrijving</label>
            <input type="text" class="col-sm-6 form-control" id="form-name" placeholder="Omschrijving" required>
          </div>

          <div class="form-group row">
            <label for="form-username" class="col-sm-2 col-form-label">Gebruikersnaam</label>
            <input type="text" class="col-sm-6 form-control" id="form-username" placeholder="Gebruiker" required>
          </div>

          <div class="form-group row">
            <label for="form-password" class="col-sm-2 col-form-label">Wachtwoord</label>
            <input type="password" class="col-sm-5 form-control" id="form-password" placeholder="Wachtwoord" required>
            <a class="btn btn-light col-sm-1 btn-showpwd" href="#"><i class="fas fa-eye"></i></a>
          </div>

          <div class="form-group row">
            <div class="col-4"><button class="btn btn-secondary" id="button-cancel" type="reset"><i class="fas fa-times"></i> Annuleren</button></div>
            <div class="col-4 pr-0"><button class="btn btn-primary float-right" id="button-save" type="submit"><i class="fas fa-check"></i> Opslaan</button></div>
          </div>
        </form>

        <script>
          $(function() {
            'use strict';

            $('#form-locations').change(function(){
              window.location = "http://www.google.nl";
            });

            $('#addpassbtn').click(function(e) {
              if($(this).hasClass('clicked')){
                $('#newpassform').slideUp('fast');
                $('#addpassbtn').removeClass('clicked');
                $('#add-username').trigger(':reset');
              }else{
                $('#newpassform').slideDown('fast');
                $('#addpassbtn').addClass('clicked');
              }
            });



            $('.btn-showpwd').click(function(e){
              e.preventDefault();

              if ($('#form-password').attr('type') == 'password')
                $('#form-password').attr('type', 'text');
              else
                $('#form-password').attr('type', 'password');
            });

            $('#addpassform').submit(function(e){
              e.preventDefault();

              $.post('ajax.addnew.php', {
                'form-category': $('#form-cat').val(),
                'form-name': $('#form-name').val(),
                'form-username': $('#form-username').val(),
                'form-location': $('#form-location').val(),
                'form-password': $('#form-password').val()
              }, function(){
                window.location.href = ".";
              });
            });

            $('#button-cancel').click(function(e){
              if($('#addpassbtn').hasClass('clicked')){
                $('#addpassbtn').removeClass('clicked');
              }
              $('#newpassform').slideUp('fast');
            });
        });
        </script>
      </div>
    </div>
    <table class='table table-striped table-hover' id="searchtable">
      <thead>
        <tr>
          <th scope="col">Omschrijving</th>
          <th scope="col">Username</th>
          <th scope="col" style="width: 150px;">Password</th>
          <th scope="col">Locatie</th>
          <th scope="col">Category</th>
          <th scope="col">Laatst aangepast</th>
          <th scope="col">Verloopt op</th>
        <?php if($currentuser->role == 3) { ?>
          <th scope="col">Actief</th>
          <th scope="col">Edit</th>
          <th scope="col">Gewijzigd door</th>
        <?php } else if($currentuser->role == 2){ ?>
          <th scope="col">Edit</th>
        <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php

        $catval = ($cat > -1) ? " AND a.`category` = :cat " : "";
        if($currentuser->role != 3) $catval .= " AND a.`active` = 1 AND l.`id` = :dept ";

        $stmt = $db->prepare("SELECT
          a.`id`,
          a.`name`,
          a.`username`,
          a.`password`,
          a.`active`,
          DATE_FORMAT(a.`lastupdated`, '%d-%m-%Y') as `lastupdated`,
          IF(c.`showdue` = 1, DATE_FORMAT(DATE_ADD(a.`lastupdated`, INTERVAL 90 DAY), '%d-%m-%Y'), '-') as `duedate`,
          a.`lastupdatedby`,
          a.`category`,
          c.`name` as `catname`,
          c.`showdue`,
          l.`name` as `locname`,
          a.`active`,
          DATE_ADD(a.`lastupdated`, INTERVAL 90 DAY) < NOW() as `verlopen`,
          DATE_ADD(a.`lastupdated`, INTERVAL 90 DAY) < DATE_ADD(NOW(), INTERVAL 7 DAY) as `bijnaverlopen`,
          COALESCE(u.`name`, '-') as `lastupdatedby`
        FROM `accounts` a
        JOIN `location` l ON a.`location` = l.`id`
        JOIN `categories` c ON a.`category` = c.`id`

        LEFT JOIN `users` u ON a.`lastupdatedby` = u.`id`
        WHERE 1=1 $catval ORDER BY a.`active` DESC, l.`name` ASC
        ");
        if ($cat > -1) $stmt->bindParam(':cat', $cat);
        if($currentuser->role != 3) $stmt->bindParam(':dept', $dept);
        $stmt->execute();

        while($row = $stmt->fetch()) {
          $verlopen = '';
          if ($row->showdue) {
            if ($row->verlopen == '1') $verlopen = 'verlopen';
            else if ($row->bijnaverlopen == '1') $verlopen = 'bijnaverlopen';
          }
           ?>
          <tr data-id="<?=$row->id?>" class="<?=$verlopen?>">
            <td>
              <i class="fas fa-exclamation-triangle text-danger error-hide"></i>
              <i class="fas fa-exclamation-circle text-warning warning-hide"></i>
               <?=$row->name?>
              <span class="badge badge-danger error-hide">verlopen</span>
              <span class="badge badge-warning warning-hide">bijna verlopen</span>
            </td>
            <td><?=$row->username?></td>
            <td class="password-field bolletjes"><?=str_repeat('&#9679;', 6)?></td>
            <td><?=$row->locname?></td>
            <td><?=$row->catname?></td>
            <td><?=$row->lastupdated?></td>
            <td><?=$row->duedate?></td>
          <?php if($currentuser->role == 3){ ?>

            <td><i class="active-check far <?=$row->active ? "fa-check-square" : "fa-square" ?>"></i></td>
            <td><a href="#" class="edit-link"><i class="fas fa-edit"></i></a></td>
            <td><?=$row->lastupdatedby?></td>
          <?php } else if($currentuser->role ==2) { ?>
            <td><a href="#" class="edit-link"><i class="fas fa-edit"></i></a></td>
          <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<script>
'use strict';
  $(function(){


    $('.edit-link').click(function(e) {
      e.preventDefault();


      var id = $(this).parent().parent().data('id');

      $('#pwdEdit .modal-content').load('ajax.edit.php', {
        'id': id,
      });

      $('#pwdEdit').modal({});
    });


    $('.password-field').click(function() {
      var $self = $(this);

      if ($self.hasClass('bolletjes')) {
        $.post('ajax.getpassword.php', {
          'id' : $self.parent().data('id')
        }, function(data) {
          $('.password-field').addClass('bolletjes').html('&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;');
          $self.removeClass('bolletjes');
          $self.text(data);
        });
      } else {
        $self.addClass('bolletjes');
        $self.html('&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;');
      }
    });

    $('#pwdEdit').on('hidden.bs.modal', function(e) {
      $('#pwdEdit .modal-content').html('<div class="loader-holder"><img src="images/loading.gif" class="loader"></div>');
    });



  });
</script>

<div class="modal fade" id="pwdEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="loader-holder"><img src="images/loading.gif" class="loader"></div>
    </div>
  </div>
</div>
