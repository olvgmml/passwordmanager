<?php
  if(!$logged_in) exit();
 ?>
     <div class="jumbotron">
       <div class="container">
         <h1 class="display-3">Hallo, <?php print($currentuser->name);?></h1>
         <p>Op deze pagina vind je informatie over het programma en hoe het werkt</p>
       </div>
     </div>

     <div class="container">
       <div class="row">
         <div class="col-md-6">
           <h2>Nieuw account aanvragen</h2>
           <p>Voor het invoeren van een nieuw account dien je contact op te nemen met je leidinggevende,
              zij kunnen via Password Manager een nieuw account toevoegen.
           </p>

         </div>
         <?php if($currentuser->role >=2){?>
         <div class="col-md-6">
           <h2>Nieuw account toevoegen</h2>
           <p>Voor een nieuw account zijn alle velden verplicht.</p>
           <p><a class="btn btn-primary" href="." role="button">Nieuw Account</a></p>
        </div>
        <?php }?>
         <div class="col-md-6">
           <h2>Account verwijderen</h2>
           <p>Om een account te laten verwijderen dien je contact op te nemen met systeembeheer</p>
           <!--<p><a class="btn btn-primary" href="mailto:e.vanleeuwen@olvg.nl?cc=j.n.m.vanleeuwen@olvg.nl;V.J.W.vanHoudt@olvg.nl;d.philipsen@olvg.nl;q.vanderlee@olvg.nl;c.w.lau@olvg.nl;p.boerebach@olvg.nl&subject=Verwijder Account" role="button">Mail Ons:</a></p>-->
           <p><a class="btn btn-primary" href="mailto:e.vanleeuwen@olvg.nl?cc=j.n.m.vanleeuwen@olvg.nl;p.boerebach@olvg.nl&subject=Verwijder Account" role="button">Mail Ons:</a></p>
         </div>

         <div class="col-md-6">
           <h2>Heading</h2>
           <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
           <p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>
         </div>
       </div>
     </div>
<hr />
