<?php if ($currentuser->role == 3) { ?>
  <div class="wrapper">
    <div class="container-fluid my-3">
      <nav class="navbar navbar-light bg-light">
        <ul class="navbar-nav mr-auto">
          <h3>Add User Log</h3>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" id="search" type="search" placeholder="Zoeken">
          <a href="#" id="clear" class="clear-hidden"><i class="fas fa-times-circle"></i></a>
        </form>
      </nav>
    </div>
  </div>
  <div class="container-left">
    <table id="searchtable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Username</th>
          <th scope="col">Name</th>
          <th scope="col">Rechten</th>
          <th scope="col">Aangemaakt op</th>
          <th scope="col">Aangemaakt door</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $stmt = $db->prepare("SELECT
            al.`username`,
            al.`name`,
            al.`userrole`,
            ur.`name` as `urname`,
            DATE_FORMAT(al.`creationdate`, '%d-%m-%Y %H:%i') as `creationdate`,
            al.`createdby`,
            u.`name` as `uname`
          FROM `adduserlog` al
          JOIN `userroles` ur ON al.`userrole` = ur.`id`
          LEFT JOIN `users` u ON al.`createdby` = u.`id`

        ");

          $stmt->execute();
          while($row = $stmt->fetch()) { ?>
          <tr data-id="<?=$row->id?>">
            <td><?=$row->username?></td>
            <td><?=$row->name?></td>
            <td><?=$row->urname?></td>
            <td><?=$row->creationdate?></td>
            <td><?=$row->uname?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
  </div>


<?php
}
 ?>
