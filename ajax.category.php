<?php
  include('inc.global.php');

  if (!$logged_in) exit();
  if (!isset($_POST['id'])) exit();

  $stmt = $db->prepare("SELECT
      c.`id`,
      c.`name`,
      c.`showdue`,
      c.`active`
    FROM
      `categories` c
    WHERE
      `id`=:id LIMIT 1");
  $stmt->bindParam(':id', $_POST['id']);
  $stmt->fetch();
  $stmt->execute();

  if ($row = $stmt->fetch()) {
    ?>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Wijzig: "<?= $row->name?>"</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="cat-form" data-id="<?=$row->id?>">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Naam:</label>
            <input type="text" class="form-control" id="cat-ncatname" value="<?=$row->name?>" required>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Actief</label>
            <input type="text" class="form-control" id="edit-active" value="<?php if($row->active == 1){ print('Actief');}else{print('Inactief');}?>" readonly>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Kan Verlopen</label>
            <input type="text" class="form-control" id="edit-candue" value="<?php if($row->showdue == 1){ print('Ja');}else{print('Nee');}?>" readonly>
          </div>

        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button id="pwdEdit-close" type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Sluit</button>
        <?php
        if($row->active == 0){ ?>
          <button id="pwdEdit-activate" type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-user-plus"></i> Activeer</button>
        <?php } else{ ?>
          <button id="pwdEdit-deactivate" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-user-minus"></i> Deactiveer</button>
        <?php }
        if($row->showdue == 0){ ?>
          <button id="pwdEdit-candue" type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-user-plus"></i> Kan Verlopen</button>
        <?php } else{ ?>
        <button id="pwdEdit-cannotdue" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-user-minus"></i> Verloopt niet</button>
        <?php } ?>
        <button id="pwdEdit-save" type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Opslaan</button>
      </div>

      <script>
      'use strict';

        $('#pwdEdit-save').click(function(e){
          e.preventDefault();

          $.post('ajax.savecategory.php', {
            'id': $('#cat-form').data('id'),
            'cat-ncatname':$('#cat-ncatname').val(),
            'cat-activate': $('#pdwEdit-activate').val(),
            'cat-name': $('#pdwEdit-deactivate').val(),
            'cat-candue': $('#pdwEdit-candue').val(),
            'cat-active': $('#pdwEdit-activate').val()
          }, function(){
            $('#pwdEdit').modal('toggle');
            location.reload();
          });
        });

      $('#pwdEdit-activate').click(function(e){
        e.preventDefault();

        $.post('ajax.activatecategory.php',{
          'id': $('#cat-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });
      $('#pwdEdit-deactivate').click(function(e){
        e.preventDefault();

        $.post('ajax.deactivatecategory.php',{
          'id': $('#cat-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });

      $('#pwdEdit-candue').click(function(e){
        e.preventDefault();

        $.post('ajax.canduecategory.php',{
          'id': $('#cat-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });

      $('#pwdEdit-cannotdue').click(function(e){
        e.preventDefault();

        $.post('ajax.cannotduecategory.php',{
          'id': $('#cat-form').data('id')
        }, function(){
          $('#pwdEdit').modal('toggle');
          location.reload();
        });
      });
      </script>
    <?php
  }
?>
