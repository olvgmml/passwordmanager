CREATE DATABASE  IF NOT EXISTS `password_manager` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `password_manager`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: password_manager
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `location` int(11) DEFAULT NULL,
  `lastupdated` date DEFAULT NULL,
  `lastupdatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'kijken of tijd wijzigt','inactieve gebruiker','inactieve_gebruiker',1,1,2,'2017-04-08',1),(2,'ajax ed','adsfgh','adghhh',2,1,3,'2019-04-29',1),(3,'een softwares','software','softwares',2,1,2,'2019-04-30',1),(4,'een algemene gebruiker','gebruikers','gebruikers',2,1,2,'2019-04-25',215501),(5,'ee nsoftware pakket','hallo','hallo',1,1,2,'2019-04-29',1),(6,'datumpje','datum','datum',2,1,4,'2019-04-29',1),(7,'adsf','adsf1','adsf33s',3,1,1,'2019-04-29',1),(8,'olvg west','gewijzigd door?','olvg westsssss',1,1,2,'2019-06-08',215501),(9,'olvg west','olvg wester','olvgwest',1,1,2,'2019-05-01',1),(11,'hoi patrick','ster','hagv',1,1,2,'2019-04-26',1),(12,'dit zou een 2 moeten zijn','blaat','blaat',3,1,2,'2019-04-26',1),(16,'log','log','log',1,1,2,'2019-04-29',NULL),(17,'lastupdatedby','lastupdatedby','lastupdatedb y',1,1,2,'2019-04-29',1);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addaccountlog`
--

DROP TABLE IF EXISTS `addaccountlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addaccountlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newusername` varchar(255) NOT NULL,
  `newpassword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `creationdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addaccountlog`
--

LOCK TABLES `addaccountlog` WRITE;
/*!40000 ALTER TABLE `addaccountlog` DISABLE KEYS */;
INSERT INTO `addaccountlog` VALUES (4,'log','log','log',2,1,1,'2019-04-29 13:05:24'),(5,'lastupdatedby','lastupdatedb y','lastupdatedby',2,1,1,'2019-04-29 13:11:55');
/*!40000 ALTER TABLE `addaccountlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `showdue` tinyint(1) NOT NULL DEFAULT '0',
  `lastupdated` date DEFAULT NULL,
  `lastupdatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Web applicaties',0,NULL,NULL),(2,'Algemene accounts',1,NULL,NULL),(3,'Software',0,NULL,NULL),(4,'test',0,'2019-05-01',1);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edituserlog`
--

DROP TABLE IF EXISTS `edituserlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edituserlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldusername` varchar(255) DEFAULT NULL,
  `newusername` varchar(255) DEFAULT NULL,
  `oldname` varchar(255) DEFAULT NULL,
  `newname` varchar(255) DEFAULT NULL,
  `oldrole` int(11) DEFAULT NULL,
  `newrole` int(11) DEFAULT NULL,
  `oldlocation` int(11) DEFAULT NULL,
  `newlocation` int(11) DEFAULT NULL,
  `edituser` int(11) DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edituserlog`
--

LOCK TABLES `edituserlog` WRITE;
/*!40000 ALTER TABLE `edituserlog` DISABLE KEYS */;
INSERT INTO `edituserlog` VALUES (1,'test','tester','hallo','geen hallo',3,2,2,1,1,'2019-01-01 11:00:00');
/*!40000 ALTER TABLE `edituserlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'OLVG Oost'),(2,'OLVG West'),(3,'Flevo Ziekenhuis'),(4,'BovenIJ');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordlog`
--

DROP TABLE IF EXISTS `passwordlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL,
  `oldusername` varchar(255) DEFAULT NULL,
  `newusername` varchar(255) DEFAULT NULL,
  `oldpassword` varchar(255) DEFAULT NULL,
  `newpassword` varchar(255) DEFAULT NULL,
  `edituser` int(11) NOT NULL,
  `editdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordlog`
--

LOCK TABLES `passwordlog` WRITE;
/*!40000 ALTER TABLE `passwordlog` DISABLE KEYS */;
INSERT INTO `passwordlog` VALUES (1,7,'adsf','adsf1','adsf','adsf2',1,'2019-04-25 15:50:13'),(2,7,'adsf1','adsf1','adsf2','adsf33',1,'2019-04-25 15:54:37'),(3,7,'adsf1','adsf1','adsf33','adsf33',1,'2019-04-26 11:07:47'),(4,7,'adsf1','adsf1','adsf33','adsf33s',1,'2019-04-26 11:08:55'),(5,7,'adsf1','adsf1','adsf33s','adsf33s',1,'2019-04-26 11:11:16'),(6,7,'adsf1','adsf1','adsf33s','adsf33s',1,'2019-04-26 11:18:15'),(7,2,'adsfgh','adsfgh','adghhh','adghhh',1,'2019-04-26 11:19:10'),(8,11,'ster','ster','ha','hagv',1,'2019-04-26 11:20:07'),(9,6,'datum','datum','datum','datum',1,'2019-04-26 14:06:36'),(10,12,'blaat','blaat','blaat','blaat',1,'2019-04-26 15:48:59'),(11,7,'adsf1','adsf1','adsf33s','adsf33s',1,'2019-04-29 09:30:33'),(12,2,'adsfgh','adsfgh','adghhh','adghhh',1,'2019-04-29 09:30:40'),(13,6,'datum','datum','datum','datum',1,'2019-04-29 09:30:47'),(14,5,'hallo','hallo','hallo','hallo',1,'2019-04-29 09:31:16'),(15,3,'software','software','softwares','softwares',1,'2019-04-30 19:39:26'),(16,9,'olvg west','olvg wester','olvgwest','olvgwest',1,'2019-05-01 09:34:03');
/*!40000 ALTER TABLE `passwordlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userroles`
--

LOCK TABLES `userroles` WRITE;
/*!40000 ALTER TABLE `userroles` DISABLE KEYS */;
INSERT INTO `userroles` VALUES (1,'Gebruiker'),(2,'Keyuser'),(3,'Beheerder');
/*!40000 ALTER TABLE `userroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `location` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` int(11) DEFAULT NULL,
  `creationdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'215501',3,'Emiel van Leeuwen',1,2,2,'2019-01-01'),(2,'227279',3,'Patrick Boerebach',1,2,3,'2019-01-01'),(3,'977687',3,'Daniel van Boeijen',1,4,4,'2019-01-01'),(4,'2731',2,'Cor Leenstra',1,3,2,'2019-01-01'),(5,'1111',3,'1111',0,1,3,'2019-01-01'),(6,'test',2,'test',1,1,1,'2019-01-01'),(7,'test',1,'test',1,2,1,'2019-01-01'),(8,'Choi',1,'choi',1,4,1,'2019-01-01'),(9,'Choi2',2,'Choi2',1,3,1,'2019-01-01'),(10,'emiel2',1,'emiel2',1,2,1,'2019-01-01'),(11,'aangemakt op',1,'aangemaakt door',1,2,1,'2019-05-01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'password_manager'
--

--
-- Dumping routines for database 'password_manager'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-01 14:49:31
