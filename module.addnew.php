<div class="container">
  <form id="editpassform" class="needs-validation mt-3">
    <div class="form-group row">
      <label for="form-cat" class="col-sm-2 col-form-label">Categorie:</label>
      <select id="form-cat" class="col-sm-10 form-control">
        <option value=""></option>
        <?php
          $stmt = $db->prepare("SELECT `id`, `name` FROM `categories` c");
          $stmt->execute();
          while($row = $stmt->fetch()) {
            ?>
            <option value="<?=$row->id?>"><?=$row->name?></option>
            <?php
          }
            ?>
      </select>
    </div>
    <div class="form-group row">
      <label for="form-name" class="col-sm-2 col-form-label">Omschrijving</label>
      <input type="text" class="col-sm-10 form-control" id="form-name" placeholder="Omschrijving">
    </div>

    <div class="form-group row">
      <label for="form-username" class="col-sm-2 col-form-label">Gebruikersnaam</label>
      <input type="text" class="col-sm-10 form-control" id="form-username" placeholder="Gebruiker" required />
    </div>

    <div class="form-group row">
      <label for="form-password" class="col-sm-2 col-form-label">Wachtwoord</label>
      <input type="password" class="col-sm-9 form-control" id="form-password" placeholder="Wachtwoord">
      <a class="btn btn-light col-sm-1 btn-showpwd" href="#"><i class="fas fa-eye"></i></a>
    </div>

    <button class="btn btn-primary" id="button-save" type="submit"><i class="fas fa-save"></i> Opslaan</button>
  </form>
</div>

<script>
  $(function(){
    'use strict';

    $('.btn-showpwd').click(function(e){
      e.preventDefault();

      if ($('#form-password').attr('type') == 'password')
        $('#form-password').attr('type', 'text');
      else
        $('#form-password').attr('type', 'password');
    });

    $('#button-save').click(function(e){
      e.preventDefault();

      $.post('ajax.addnew.php', {
        'form-category': $('#form-cat').val(),
        'form-name': $('#form-name').val(),
        'form-username': $('#form-username').val(),
        'form-password': $('#form-password').val()
      }, function(){
        window.location.href = ".";
      });
    });
  });
</script>
